import React from "react"
import Card from "./card"

const Recipes = ({ recipes }) => {
  const leftRecipesCount = Math.ceil(recipes.length / 5)
  const leftRecipes = recipes.slice(0, leftRecipesCount)
  const rightRecipes = recipes.slice(leftRecipesCount, recipes.length)

  return (
    <div>
      <div className="uk-child-width-1-2" data-uk-grid>
        <div>
          {leftRecipes.map((recipe, i) => {
            return (
              <Card recipe={recipe} key={`recipe__${recipe.node.strapiId}`} />
            )
          })}
        </div>
        <div>
          <div className="uk-child-width-1-2@m uk-grid-match" data-uk-grid>
            {rightRecipes.map((recipe, i) => {
              return (
                <Card recipe={recipe} key={`recipe__${recipe.node.strapiId}`} />
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Recipes