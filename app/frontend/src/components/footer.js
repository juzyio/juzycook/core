import React from "react";

const Footer = () => (
	<div className="h-48 bg-gray-800">
		<div className="flex h-24 text-center items-center">
			<div className="text-white mx-auto">All right reserved 2020.</div>
		</div>
	</div>
);

export default Footer;
