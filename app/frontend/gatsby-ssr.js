/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

var React = require("react");

exports.onPreRenderHTML = ({ getHeadComponents, replaceHeadComponents }) => {
	/**
	 * @type {any[]} headComponents
	 */
	const headComponents = getHeadComponents();

	headComponents.sort((a, b) => {
		if (a.props && a.props["data-react-helmet"]) {
			return 0;
		}
		return 1;
	});
	replaceHeadComponents(headComponents);
};
