module.exports = {
	theme: {
		fontFamily: {
			baloo: ['"Baloo Chettan 2"'],
		},
		extend: {
			maxHeight: {
				xs: "20rem",
				sm: "24rem",
				md: "28rem",
				lg: "32rem",
				xl: "36rem",
			},
		},
	},
	variants: {},
	plugins: [],
};
