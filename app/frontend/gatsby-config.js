require("dotenv").config({
	path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
	siteMetadata: {
		title: `Juzycook 食餐好`,
		description: `今晚食咩好？由食材到菜式風格，搜尋出小家庭家常菜、粥粉麵飯、糖水食譜簡單住家飯。`,
		author: `@JuChu`,
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
				trackingId: "UA-163924503-3",
				head: true,
				// Avoids sending pageview hits from custom paths
				// exclude: ["/preview/**", "/do-not-track/me/too/"],
				// cookieDomain: "example.com",
			},
		},
		{
			resolve: "gatsby-plugin-postcss",
			options: {
				postCssPlugins: [
					require("tailwindcss")("./tailwind.config.js"),
				],
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`,
			},
		},
		{
			resolve: `gatsby-source-strapi`,
			options: {
				apiURL: `${process.env.STRAPI_ENDPOINT}`,
				queryLimit: 1000, // Default to 100
				contentTypes: [
					// List of the Content Types you want to be able to request from Gatsby.
					"recipe",
					"category",
					"style",
				],
			},
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `Juzycook recipe site`,
				short_name: `juzycook`,
				start_url: `/`,
				background_color: `#ffdf00`,
				theme_color: `#ffdf00`,
				display: `minimal-ui`,
				icon: `src/images/juzycook-icon.png`, // This path is relative to the root of the site.
			},
		},
		{
			resolve: `gatsby-plugin-prefetch-google-fonts`,
			options: {
				fonts: [
					{
						family: `Baloo Chettan 2`,
						variants: [`800`],
					},
				],
			},
		},
		{
			resolve: `gatsby-plugin-purgecss`,
			options: {
				printRejected: true, // Print removed selectors and processed file names
				// develop: true, // Enable while using `gatsby develop`
				tailwind: true, // Enable tailwindcss support
				// whitelist: ['whitelist'], // Don't remove this selector
				// ignore: ['/ignored.css', 'prismjs/', 'docsearch.js/'], // Ignore files/folders
				// purgeOnly : ['components/', '/main.css', 'bootstrap/'], // Purge only these files/folders
			},
		},
		// this (optional) plugin enables Progressive Web App + Offline functionality
		// To learn more, visit: https://gatsby.dev/offline
		// `gatsby-plugin-offline`,
	],
};
