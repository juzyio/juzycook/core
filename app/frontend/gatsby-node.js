/**
 * Function for generate pages during build
 */
exports.createPages = async ({ graphql, actions }) => {
	const { createPage } = actions;
	const result = await graphql(
		`
			{
				recipes: allStrapiRecipe(
					filter: { Publish: { eq: true } }
					sort: { fields: [Sorting, updatedAt], order: DESC }
				) {
					edges {
						node {
							strapiId
							Slug
						}
					}
				}
				categories: allStrapiCategory {
					edges {
						node {
							strapiId
							Slug
						}
					}
				}
				styles: allStrapiStyle {
					edges {
						node {
							strapiId
							Slug
						}
					}
				}
			}
		`
	);

	if (result.errors) {
		throw result.errors;
	}

	// Create recipes pages.
	const recipes = result.data.recipes.edges;
	const categories = result.data.categories.edges;
	const styles = result.data.styles.edges;

	recipes.forEach((recipe, index) => {
		createPage({
			path: `/recipe/${recipe.node.Slug}`,
			component: require.resolve("./src/templates/recipe.js"),
			context: {
				id: recipe.node.strapiId,
			},
		});
	});

	categories.forEach((category, index) => {
		createPage({
			path: `/category/${category.node.Slug}`,
			component: require.resolve("./src/templates/category.js"),
			context: {
				id: category.node.strapiId,
			},
		});
	});

	styles.forEach((style, index) => {
		createPage({
			path: `/style/${style.node.Slug}`,
			component: require.resolve("./src/templates/style.js"),
			context: {
				id: style.node.strapiId,
			},
		});
	});
};

/**
 * Function for generate search index
 */
const { GraphQLJSONObject } = require(`graphql-type-json`);
const path = require(`path`);
const lunr = require(`lunr`);
require(`./lib/lunr-languages/lunr.stemmer.support`)(lunr);
require(`./lib/lunr-languages/lunr.zh`)(lunr, {
	dict: path.join(__dirname, `/src/config/lunr-lang-dict/zhtwdict.txt`),
	userDict: path.join(__dirname, `/src/config/lunr-lang-dict/userdict.txt`),
});

exports.createResolvers = ({ cache, createResolvers }) => {
	createResolvers({
		Query: {
			RecipeIndex: {
				type: GraphQLJSONObject, // Index object will be a JSON object
				resolve: (source, args, context, info) => {
					// Retrieve entire recipe list
					const recipeNodes = context.nodeModel.getAllNodes({
						type: `StrapiRecipe`,
					});
					const type = info.schema.getType(`StrapiRecipe`);
					return createIndex(recipeNodes, type, cache);
				},
			},
		},
	});
};

const createIndex = async (recipeNodes, type, cache) => {
	const documents = [];

	// Iterate over all recipe to prepare document
	console.log("start building index");
	for (const node of recipeNodes) {
		if (node.Publish == false) continue;
		const slug = node.Slug;
		const name = node.Name;
		const categoriesName = [];
		node.categories.forEach((element) => {
			categoriesName.push(element["Name"]);
		});
		const stylesName = [];
		node.styles.forEach((element) => {
			stylesName.push(element["Name"]);
		});
		const ingredientsName = [];
		node.IngredientServing.forEach((element) => {
			ingredientsName.push(element["ingredient"]["Name"]);
		});
		documents.push({
			// id: node.id,
			slug: slug,
			name: name,
			categoryName: categoriesName,
			styleName: stylesName,
			ingredientName: ingredientsName,
		});
	}

	// Additional trimmer
	// const improvedTrimmer = function (builder) {
	// 	const pipelineFunction = function (token) {
	// 		return token.update(function (s) {
	// 			return s.replace(/[（）]/, "");
	// 		});
	// 	};

	// Register the pipeline function so the index can be serialized
	// lunr.Pipeline.registerFunction(pipelineFunction, "improvedTrimmer");

	// Add the pipeline function to both the indexing pipeline and the searching pipeline
	// builder.pipeline.after(lunr.zh, pipelineFunction);
	// console.log("3");
	// builder.pipeline.remove(lunr.trimmer);
	// builder.searchPipeline.before(lunr.trimmer, pipelineFunction);
	// builder.searchPipeline.remove(lunr.trimmer);
	// };

	// const trimPipeline = function (token) {
	// 	return token.update(function (s) {
	// 		return s.replace(/[（）]/, "");
	// 	});
	// };
	// // // Register the pipeline function so the index can be serialized
	// lunr.Pipeline.registerFunction(trimPipeline, "trimPipeline");

	// Build index by adding documents
	const index = lunr(function () {
		this.use(lunr.zh);
		// this.use(trimPipeline);
		// this.use(improvedTrimmer);
		// this.pipeline.add(improvedTrimmer);
		// console.log("1");
		// this.searchPipeline.add(improvedTrimmer);
		// console.log("2");

		// this.ref(`id`);
		this.ref(`slug`);
		this.field(`name`, { boost: 10 });
		this.field(`categoryName`);
		this.field(`styleName`);
		this.field(`ingredientName`);
		this.metadataWhitelist = ["position"];
		for (const doc of documents) {
			this.add(doc);
		}
	});

	console.log(index.toJSON());
	return index.toJSON();
};
